### Stage 1: Setup base python with requirements
FROM python:3.7-slim-buster AS base-layer

COPY requirements/base.txt /tmp

# Add necessary packages to system
RUN apt-get update && \
    apt-get install -y git libmagic1 gcc && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    cd /tmp && \
    pip install -r base.txt && \
    apt-get autoremove -y --purge gcc

# Setup application
COPY . /opt/minty
WORKDIR /opt/minty

FROM base-layer AS production

ENV OTAP=production

ENV PYTHONUNBUFFERED=on

FROM base-layer AS quality-and-testing

COPY requirements/test.txt /tmp
RUN pip install -r /tmp/test.txt

WORKDIR /opt/minty

RUN bin/generate_documentation.sh

FROM quality-and-testing AS development

ENV OTAP=development

COPY requirements/dev.txt /tmp
RUN pip install -r /tmp/dev.txt
