import pytest

from minty.logging import mdc


class TestMDC:
    def test_set_mdc(self):
        with pytest.raises(AttributeError):
            getattr(mdc._mdc, "test")

        mdc.set_mdc("test", "value")
        assert getattr(mdc._mdc, "test") == "value"

    def test_get_mdc(self):
        mdc.clear_mdc()

        assert mdc.get_mdc() == {}
        mdc.set_mdc("test", "value")
        mdc.set_mdc("test2", "value2")
        assert mdc.get_mdc() == {"test": "value", "test2": "value2"}

        # Bonus: test clearing the MDC
        mdc.clear_mdc()
        assert mdc.get_mdc() == {}

    def test_contextmanager(self):
        mdc.clear_mdc()
        assert mdc.get_mdc() == {}
        with mdc.mdc(foo="bar"):
            assert mdc.get_mdc() == {"foo": "bar"}
        assert mdc.get_mdc() == {}
