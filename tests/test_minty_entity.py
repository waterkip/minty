from datetime import datetime, timezone

from minty.entity import EntityBase, _reflect


class MockEntity(EntityBase):
    def __init__(self):
        self.id = "id-12345"

    @property
    def entity_id(self):
        return self.id


class TestEventDecorator:
    def test_reflect(self):
        assert _reflect(value=None) is None
        assert _reflect(value=True) is True
        assert _reflect(value=10) == 10
        assert _reflect(value=10) == 10

        mock_ent = MockEntity()
        assert _reflect(value=mock_ent) == {
            "type": "MockEntity",
            "entity_id": "id-12345",
        }
        assert _reflect(value=[1, 2, 3, 4, True]) == [1, 2, 3, 4, True]
        assert _reflect(value=[[1, 2], [3, 4], [True]]) == [
            [1, 2],
            [3, 4],
            [True],
        ]
        assert _reflect(value=[mock_ent, 2, 3, 4, True]) == [
            {"type": "MockEntity", "entity_id": "id-12345"},
            2,
            3,
            4,
            True,
        ]
        assert _reflect(value=[[mock_ent, 2], 3, 4, True]) == [
            [{"type": "MockEntity", "entity_id": "id-12345"}, 2],
            3,
            4,
            True,
        ]
        assert _reflect(value=[{"k": mock_ent}, 2, 3, 4, True]) == [
            {"k": {"type": "MockEntity", "entity_id": "id-12345"}},
            2,
            3,
            4,
            True,
        ]
        assert _reflect(value={"m": mock_ent}) == {
            "m": {"type": "MockEntity", "entity_id": "id-12345"}
        }

        dt = datetime.now(timezone.utc)
        assert _reflect(dt) == dt.isoformat()
