from unittest import mock

from minty.repository import RepositoryBase


class MockRepository(RepositoryBase):
    pass


class TestRepositoryBase:
    def test_repository_base(self):
        infra_factory = mock.MagicMock()
        infra_factory.get_infrastructure.return_value = "bar"
        r = MockRepository(infra_factory, context="c", event_service="es")

        rv = r._get_infrastructure("foo")
        assert rv == "bar"

        infra_factory.get_infrastructure.assert_called_once_with(
            context="c", infrastructure_name="foo"
        )
