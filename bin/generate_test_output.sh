#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../"

mkdir -p /tmp/tests && pytest -m "not live" --junitxml=/tmp/tests/junit.xml --cov=minty --cov-report=term
